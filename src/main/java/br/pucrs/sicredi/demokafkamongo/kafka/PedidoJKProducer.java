package br.pucrs.sicredi.demokafkamongo.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import br.pucrs.sicredi.demokafkamongo.dto.PedidoDTO;

@Component
public class PedidoJKProducer {
    @Value("${spring.kafka.pedido-topico.name}")
    private String pedidoTopico;
    private KafkaTemplate<String, PedidoDTO> pedidoTemplate;

    public PedidoJKProducer(KafkaTemplate<String, PedidoDTO> template){
        this.pedidoTemplate = template;
    }
    
    public void enviaPedido(PedidoDTO pedDto){
        Message<PedidoDTO> msg = MessageBuilder.withPayload(pedDto).
                                                    setHeader(KafkaHeaders.TOPIC, pedidoTopico).
                                                    build();

        pedidoTemplate.send(msg);
    }
}
