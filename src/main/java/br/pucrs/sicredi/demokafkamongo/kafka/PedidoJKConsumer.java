package br.pucrs.sicredi.demokafkamongo.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import br.pucrs.sicredi.demokafkamongo.dto.PedidoDTO;
import br.pucrs.sicredi.demokafkamongo.model.Pedido;
import br.pucrs.sicredi.demokafkamongo.repository.PedidoRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class PedidoJKConsumer {
    private PedidoRepository pedRepo;

    @KafkaListener(topics = "${spring.kafka.pedido-topico.name}", groupId = "${spring.kafka.consumer.group-id}")
    public void consomePedido(PedidoDTO pedDto) {
        Pedido ped = new Pedido(pedDto.getCodCliente(), pedDto.getItens());
        pedRepo.insert(ped);
    }
}
