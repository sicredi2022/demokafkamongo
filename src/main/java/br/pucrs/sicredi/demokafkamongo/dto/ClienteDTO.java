package br.pucrs.sicredi.demokafkamongo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO {
    private String codCliente;
    private String nomeCliente;
}
