package br.pucrs.sicredi.demokafkamongo.service;

import org.springframework.stereotype.Service;

import br.pucrs.sicredi.demokafkamongo.dto.PedidoDTO;
import br.pucrs.sicredi.demokafkamongo.kafka.PedidoJKProducer;
import br.pucrs.sicredi.demokafkamongo.model.Cliente;
import br.pucrs.sicredi.demokafkamongo.model.Produto;
import br.pucrs.sicredi.demokafkamongo.repository.ClienteRepository;
import br.pucrs.sicredi.demokafkamongo.repository.ProdutoRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class PedidoProducerService {
    private PedidoJKProducer pedProducer;
    private ClienteRepository cliRepo;
    private ProdutoRepository proRepo;

    public void enviaPedido(PedidoDTO pedDto){
        Cliente cli = null;
        Produto pro = null;

        if (pedDto == null)
            throw new IllegalArgumentException("Pedido inválido!");
        
        cli = cliRepo.findByCodCliente(pedDto.getCodCliente());
        if (cli == null)
            throw new IllegalArgumentException("Cliente inválido no pedido!");

        for(String aux: pedDto.getItens()){
            pro = proRepo.findByCodProduto(aux);
            if (pro == null)
                throw new IllegalArgumentException("Item inválido!");
        }

        pedProducer.enviaPedido(pedDto);
    }



    
    
}
