package br.pucrs.sicredi.demokafkamongo.service;

import org.springframework.stereotype.Service;

import br.pucrs.sicredi.demokafkamongo.dto.ProdutoDTO;
import br.pucrs.sicredi.demokafkamongo.model.Produto;
import br.pucrs.sicredi.demokafkamongo.repository.ProdutoRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class ProdutoService {

    private ProdutoRepository proRepo;

    public Produto cadatraProduto(ProdutoDTO pro) {
        Produto proModel = null;

        if (pro != null){
            Produto aux = proRepo.findByCodProduto(pro.getCodProduto());

            if (aux != null)
                throw new IllegalArgumentException("Produto já existe!");

            proModel = new Produto(pro.getCodProduto(), pro.getNomeProduto());

            proRepo.insert(proModel);
        }

        return proModel;
    }
    
      
    public Produto buscaProduto(String cod) {
        Produto pro = null;

        pro = proRepo.findByCodProduto(cod);
        if (pro == null)
            throw new IllegalArgumentException("Produto não encontrado!");

        return pro;
    }
}
