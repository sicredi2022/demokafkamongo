package br.pucrs.sicredi.demokafkamongo.service;

import org.springframework.stereotype.Service;

import br.pucrs.sicredi.demokafkamongo.dto.ClienteDTO;
import br.pucrs.sicredi.demokafkamongo.model.Cliente;
import br.pucrs.sicredi.demokafkamongo.repository.ClienteRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class ClienteService {

    private ClienteRepository cliRepo;

    public Cliente cadatraCliente(ClienteDTO cli) {
        Cliente cliModel = null;

        if (cli != null){
            Cliente aux = cliRepo.findByCodCliente(cli.getCodCliente());

            if (aux != null)
                throw new IllegalArgumentException("Cliente já existe!");

            cliModel = new Cliente(cli.getCodCliente(), cli.getNomeCliente());

            cliRepo.insert(cliModel);
        }

        return cliModel;
    }
    
    public Cliente buscaCliente(String cod) {
        Cliente cli = null;

        cli = cliRepo.findByCodCliente(cod);
        if (cli == null)
            throw new IllegalArgumentException("Cliente não encontrado!");

        return cli;
    }
}
