package br.pucrs.sicredi.demokafkamongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.sicredi.demokafkamongo.model.Pedido;

public interface PedidoRepository extends MongoRepository<Pedido, String> {

}

