package br.pucrs.sicredi.demokafkamongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.sicredi.demokafkamongo.model.Produto;

public interface ProdutoRepository extends MongoRepository<Produto, String> {
    public Produto findByCodProduto(String cod);
}
