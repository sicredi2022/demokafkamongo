package br.pucrs.sicredi.demokafkamongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.sicredi.demokafkamongo.model.Cliente;

public interface ClienteRepository extends MongoRepository<Cliente, String> {
    public Cliente findByCodCliente(String cod);
}
