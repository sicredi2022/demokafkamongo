package br.pucrs.sicredi.demokafkamongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemokafkamongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemokafkamongoApplication.class, args);
	}

}
