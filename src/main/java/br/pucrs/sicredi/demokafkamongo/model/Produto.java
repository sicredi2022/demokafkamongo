package br.pucrs.sicredi.demokafkamongo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "produto")
public class Produto {
    @Id
    private String codProduto;
    private String nomeProduto;
}
