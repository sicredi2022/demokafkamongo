package br.pucrs.sicredi.demokafkamongo.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document(collection = "pedidos")
public class Pedido {
    private String codCliente;
    private List<String> itens;
    
}
