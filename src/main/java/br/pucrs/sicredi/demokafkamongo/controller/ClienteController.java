package br.pucrs.sicredi.demokafkamongo.controller;

import java.net.URI;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.pucrs.sicredi.demokafkamongo.dto.ClienteDTO;
import br.pucrs.sicredi.demokafkamongo.model.Cliente;
import br.pucrs.sicredi.demokafkamongo.service.ClienteService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class ClienteController {

    private ClienteService cliService;

    @PostMapping("/cliente/add")
    public ResponseEntity<Void> cadastraCliente(@RequestBody ClienteDTO cliDto){
        Cliente cli = cliService.cadatraCliente(cliDto);

        if (cli == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(cli.getCodCliente()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/cliente/busca/{codigo}")
    public ResponseEntity<Cliente> buscaCliente(@PathVariable String codigo){
        Cliente cli = null;

        cli = cliService.buscaCliente(codigo);

        return ResponseEntity.ok(cli);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException excp){
        return ResponseEntity.
            status(HttpStatus.EXPECTATION_FAILED).
            body(excp.getMessage());
    }
}
