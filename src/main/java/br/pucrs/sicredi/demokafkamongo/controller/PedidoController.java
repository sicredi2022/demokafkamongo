package br.pucrs.sicredi.demokafkamongo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.pucrs.sicredi.demokafkamongo.dto.PedidoDTO;
import br.pucrs.sicredi.demokafkamongo.service.PedidoProducerService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class PedidoController {

    private PedidoProducerService pedService;

    @PostMapping("/pedido/add")
    public ResponseEntity<String> cadastraPedido(@RequestBody PedidoDTO pedDto){
        pedService.enviaPedido(pedDto);
        return ResponseEntity.ok("Pedido criado!");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException excp){
        return ResponseEntity.
            status(HttpStatus.EXPECTATION_FAILED).
            body(excp.getMessage());
    }
}
