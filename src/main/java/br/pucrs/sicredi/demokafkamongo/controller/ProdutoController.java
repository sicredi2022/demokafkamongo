package br.pucrs.sicredi.demokafkamongo.controller;

import java.net.URI;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.pucrs.sicredi.demokafkamongo.dto.ProdutoDTO;
import br.pucrs.sicredi.demokafkamongo.model.Produto;
import br.pucrs.sicredi.demokafkamongo.service.ProdutoService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class ProdutoController {

    private ProdutoService proService;

    @PostMapping("/produto/add")
    public ResponseEntity<Void> cadastraProduto(@RequestBody ProdutoDTO proDto){
        Produto pro = proService.cadatraProduto(proDto);

        if (pro == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(pro.getCodProduto()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/produto/busca/{codigo}")
    public ResponseEntity<Produto> buscaProduto(@PathVariable String codigo){
        Produto pro = null;

        pro = proService.buscaProduto(codigo);

        return ResponseEntity.ok(pro);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException excp){
        return ResponseEntity.
            status(HttpStatus.EXPECTATION_FAILED).
            body(excp.getMessage());
    }
}
